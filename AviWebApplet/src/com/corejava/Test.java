package com.corejava;

public class Test {

	public static void main(String[] args) {
		String name = "Shiba Sankar Adak";
		String lastName = "";
		String firstName = "";
		if (name.split("\\w+").length > 1) {

			lastName = name.substring(name.lastIndexOf(" ") + 1);
			firstName = name.substring(0, name.lastIndexOf(' '));
		} else {
			firstName = name;
		}
		
		System.out.println("first name: "+ firstName);
		System.out.println("last name: "+ lastName);
	}
}
